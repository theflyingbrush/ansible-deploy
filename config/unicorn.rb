# Set the working application directory
# working_directory "/path/to/your/app"

app_dir = File.realpath(".")

working_directory app_dir

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "#{app_dir}/tmp/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "#{app_dir}/log/unicorn.log"
stdout_path "#{app_dir}/log/unicorn.log"

# Unicorn socket
listen "#{app_dir}/tmp/sockets/unicorn.sock"
listen "0.0.0.0:5050", :tcp_nopush => true

# Number of processes
# worker_processes 4
worker_processes 2

# Time-out
timeout 30